<?php

use App\Http\Controllers\CastController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TableController;
use App\Http\Controllers\DataTableController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('layouts.master');
// });
// Route::get('/', HomeController:"index");
Route::get('/', [HomeController::class, 'index']);
Route::get('table', [TableController::class, 'index']);
Route::get('data-table', [DataTableController::class, 'index']);

// menampilkan list data para pemain film (boleh menggunakan table html atau bootstrap card)
Route::get('/cast', [CastController::class, 'index']);
// menampilkan form untuk membuat data pemain film baru
Route::get('/cast/create', [CastController::class, 'create']);
// menyimpan data baru ke tabel Cast
Route::post('/cast', [CastController::class, 'simpan']);
// menampilkan detail data pemain film dengan id tertentu
Route::get('/cast/{cast_id}', [CastController::class, 'show']);
// menampilkan form untuk edit pemain film dengan id tertentu
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
// menyimpan perubahan data pemain film (update) untuk id tertentu
Route::put('/cast/{cast_id}', [CastController::class, 'update']);
// menghapus data pemain film dengan id tertentu
Route::delete('/kategori/{kategori_id}', [CastController::class, 'destroy']);
