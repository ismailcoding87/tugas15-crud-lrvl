
@extends('layouts.master')
@section('judul')
Halaman Detail
@endsection

@section('content')
<div class="container">
    <div class="h4">{{$cast->nama}} {{$cast->umur}} tahun</div>
    <p>{{$cast->bio}}</p>
</div>
@endsection

