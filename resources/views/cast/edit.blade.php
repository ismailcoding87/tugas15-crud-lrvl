
@extends('layouts.master')
@section('judul')
Halaman Edit
@endsection

@section('content')
<div class="container">
    <form action="/cast/{{$cast->id}}" method="POST">
        @method("PUT")
        @csrf
        <div class="mb-3">
            <label for="nama" class="form-label">Nama</label>
            <input name="nama" type="name" class="form-control" id="nama" value="{{$cast->nama}}">
          </div>
          @error('nama')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
          <div class="mb-3">
            <label for="umur" class="form-label">Umur</label>
            <input name="umur" type="number" class="form-control" id="umur"  value="{{$cast->umur}}">
          </div>
          @error('umur')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
          <div class="mb-3">
            <label for="umur" class="form-label">Bio</label>
            <textarea class="form-control" name="bio" id="bio" cols="20" rows="5">{{$cast->bio}}</textarea>
          </div>
          @error('bio')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
          <div class="mt-4">
                <button type="submit" class="btn btn-success w-100">Simpan</button> 
          </div>
    </form>  
</div>
@endsection

