
@extends('layouts.master')
@section('judul')
Halaman Cast
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm">Tambah Data</a>

<table class="table table-striped table-sm">
<thead>
    <tr>
        <td>No</td>
        <td>Nama</td>
        <td>Action</td>
    </tr>
</thead>
<tbody>
    @forelse ($cast as $key => $value)
    <tr>
            <td>{{$key + 1}}</td>
            <td>{{$value->nama}}</td>
            <td>
                <form action="/kategori/{{$value->id}}" method="POST">
                    @csrf
                    @method("delete")
                    <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <button type="submit" class="btn btn-danger btn-sm">Hapus</button>
                </form>
               
            </td>
    </tr>
@empty
    <p>No users</p>
@endforelse
</tbody>
  </table>
@endsection

